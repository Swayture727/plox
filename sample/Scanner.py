from typing import Dict, Final, List, Optional


from Token import LiteralValue, Token, TokenType
from errorReport import scanner_error


class Scanner:
    _source: Final[str]
    # TODO: Add final, removed for testing Final[List[Token]]
    _tokens: List[Token] = []
    _start: int = 0
    _current: int = 0
    _line: int = 0
    _col: int = 0
    keywords: Final[Dict[str, TokenType]] = {
        "and": TokenType.AND,
        "class": TokenType.CLASS,
        "else": TokenType.ELSE,
        "false": TokenType.FALSE,
        "for": TokenType.FOR,
        "fun": TokenType.FUN,
        "if": TokenType.IF,
        "nil": TokenType.NIL,
        "or": TokenType.OR,
        "print": TokenType.PRINT,
        "return": TokenType.RETURN,
        "super": TokenType.SUPER,
        "this": TokenType.THIS,
        "true": TokenType.TRUE,
        "var": TokenType.VAR,
        "while": TokenType.WHILE,
    }

    def __init__(self, source) -> None:
        self._source = source

    def _is_at_end(self) -> bool:
        return self._current >= len(self._source)

    def scan_tokens(self) -> List[Token]:
        self._tokens = []
        while not self._is_at_end():
            self._scan_token()
            self._start = self._current
        self._tokens.append(Token(TokenType.EOF, "", None, self._line, self._col))
        return self._tokens

    def _look_at_next(self, c: str) -> bool:
        if self._is_at_end():
            return False
        if self._source[self._current] != c:
            return False
        self._current += 1
        self._col += 1
        return True

    def _peek(self) -> Optional[str]:
        if self._is_at_end():
            return None
        return self._source[self._current]

    def _scan_token(self):
        c: str = self._advance()
        match (c):
            case "(":
                self._add_token_type(TokenType.LEFT_PAREN)
            case ")":
                self._add_token_type(TokenType.RIGHT_PAREN)
            case "{":
                self._add_token_type(TokenType.LEFT_BRACE)
            case "}":
                self._add_token_type(TokenType.RIGHT_BRACE)
            case ",":
                self._add_token_type(TokenType.COMMA)
            case ".":
                self._add_token_type(TokenType.DOT)
            case "-":
                self._add_token_type(TokenType.MINUS)
            case "+":
                self._add_token_type(TokenType.PLUS)
            case ";":
                self._add_token_type(TokenType.SEMICOLON)
            case "*":
                self._add_token_type(TokenType.STAR)
            # TODO: blob a sequence of bad characters together
            case "!":
                if self._look_at_next("="):
                    self._add_token_type(TokenType.BANG_EQUAL)
                else:
                    self._add_token_type(TokenType.BANG)
                pass
            case ">":
                if self._look_at_next("="):
                    self._add_token_type(TokenType.GREATER_EQUAL)
                else:
                    self._add_token_type(TokenType.GREATER)
            case "=":
                if self._look_at_next("="):
                    self._add_token_type(TokenType.EQUAL_EQUAL)
                else:
                    self._add_token_type(TokenType.EQUAL)
            case "<":
                if self._look_at_next("="):
                    self._add_token_type(TokenType.LESSER_EQUAL)
                else:
                    self._add_token_type(TokenType.LESSER)
            case "/":
                if self._look_at_next("/"):
                    while self._peek() != "\n" and not self._is_at_end():
                        self._advance()
                else:
                    self._add_token_type(TokenType.SLASH)
            case " ":
                pass
            case "\r":
                pass
            case "\t":
                pass
            case "\n":
                self._line += 1
                self._col = 0
            case '"':
                self._parse_string()
            case _ if c.isdigit():
                self._parse_num()
            case _ if c.isalpha() or c == "_":
                self._identifier()
                return
            case _:
                self._report_error(f"Unexpected character {c}")
        self._col += 1

    def _find_end_of_line(self) -> int:
        end_of_line = self._col
        source_len = len(self._source)
        while end_of_line >= source_len and self._source[end_of_line] != "\n":
            end_of_line += 1
        return end_of_line

    def _get_line_str(self) -> str:
        return self._source[
            self._current - self._col - 1 : self._current + self._find_end_of_line()
        ]

    def _report_error(self, msg: str):
        scanner_error(self._line, self._col, msg, self._get_line_str())

    # TODO: Maybe add a bool or something to see whether this failed or not?
    # perhaps it might not matter, not sure. Applies to _parse_string aswell
    def _parse_num(self) -> None:
        isFloat = False
        peeked = self._peek()
        while not self._is_at_end() and peeked and (peeked.isdigit() or peeked == "."):
            if peeked == ".":
                if isFloat:
                    self._advance()
                    self._report_error(
                        f"Unexpected character(this number seems to contain two dots)"
                    )
                    return
                isFloat = True
            self._advance()
            self._col += 1
            peeked = self._peek()
        if self._source[self._current - 1] == ".":
            self._report_error(f"Unexpected character(a trailing dot detected)")
            self._advance()
            self._col += 1
            return
        literal: LiteralValue
        if isFloat:
            literal = float(self._source[self._start : self._current])
        else:
            literal = int(self._source[self._start : self._current])

        self._add_token(type=TokenType.NUMBER, literal=literal)

    def _parse_string(self) -> None:
        while self._peek() != '"' and not self._is_at_end():
            if self._peek() == "\n":
                self._line += 1
            self._advance()
            self._col += 1
        if self._is_at_end():
            self._report_error(f"Unterminated string literal")
            return
        self._advance()
        self._col += 1
        self._add_token(
            type=TokenType.STRING,
            literal=self._source[self._start + 1 : self._current - 1],
        )

    def _identifier(self):
        peeked = self._peek()
        while not self._is_at_end() and peeked and (peeked.isalnum() or peeked == "_"):
            self._advance()
            peeked = self._peek()
        identifier = self._source[self._start : self._current]
        if identifier in self.keywords:
            self._add_token_type(self.keywords[identifier])
        else:
            self._add_token_type(TokenType.IDENTIFIER)
        self._col += len(identifier)

    def _advance(self) -> str:
        if self._is_at_end():
            return self._source[self._current - 1]
        char = self._source[self._current]
        self._current += 1
        return char

    def _add_token_type(self, type: TokenType) -> None:
        self._add_token(type, None)

    def _add_token(self, type: TokenType, literal: LiteralValue) -> None:
        text = self._source[self._start : self._current]
        self._tokens.append(
            Token(type, text, literal, self._line, self._col - len(text))
        )
