import argparse
from typing import Optional
from Scanner import Scanner
from Parser import Parser
from Expr import AstPrinter, Interpreter, LoxRuntimeError
from errorReport import is_error

PROGRAM_DESCRIPTION = """Python interpreter for Lox programming language,
                   made as a follow along with the book crafting interpreters by Robert Nystrom."""

# TODO: change from this dirty global to something actually human
had_runtime_error: bool = False
interpreter: Interpreter = Interpreter()


def parse_args() -> Optional[str]:
    parser = argparse.ArgumentParser(description=PROGRAM_DESCRIPTION)
    parser.add_argument(
        "script", metavar="script", nargs="?", help="A script that is to be run."
    )
    args = parser.parse_args()
    return args.script


def run_file(fileName: str):
    with open(fileName, "r", encoding="utf-8") as file:
        run(file.read())
        if is_error():
            exit(65)
        global had_runtime_error
        if had_runtime_error:
            exit(70)


def run_prompt():
    while True:
        try:
            line = input("> ")
        except EOFError:
            break
        try:
            run(line)
        except LoxRuntimeError as e:
            print(e)


def run(program: str):
    scanner = Scanner(program)
    tokens = scanner.scan_tokens()
    parser: Parser = Parser(tokens)
    expr = parser.parse()
    if is_error():
        exit(65)
    assert expr != None
    global interpreter
    interpreter.interpret(expr)


def runtimeError(error: LoxRuntimeError) -> None:
    print(f"{error.msg} \n[line {error.token.line}:{error.token.column}]")
    global had_runtime_error
    had_runtime_error = True


if __name__ == "__main__":
    fileName = parse_args()
    if fileName:
        run_file(fileName)
    else:
        run_prompt()
    pass
