# Unexcpected character '@' at line 180:25
# 180 | var foo @= 50 + ...
#               ^--Here
from Token import Token, TokenType


# TODO: change from this dirty global to something actually human, this is temporary

had_error = False

def is_error() -> bool:
    return had_error

def scanner_error(line_num: int, column: int, message: str, lineStr: str) -> None:
    # TODO: change from this dirty global to something actually human, this is temporary
    global had_error
    had_error = True
    err_len = 80
    line_number_str = f"{line_num}:{column} | "
    left_boundary = column
    if err_len < len(lineStr) + len(line_number_str):
        if column < err_len:
            lineStr = lineStr[: err_len - len(line_number_str)]
        else:
            right_bounadry = column * 2 if len(lineStr) > column * 2 else len(lineStr)
            left_boundary = column // 2
            lineStr = lineStr[left_boundary:right_bounadry]
    print(message)
    print(f"{line_number_str}{lineStr}")
    print(" " * (len(line_number_str) + left_boundary + 1), end="")
    print("^--Here")


def parser_error(token: Token, msg: str) -> None:
    # TODO: change from this dirty global to something actually human, this is temporary
    global had_error
    had_error = True
    if token.type == TokenType.EOF:
        parser_report(token.line, token.column, " at end", msg)
    else:
        parser_report(token.line, token.column, f" at '{token.lexeme}'", msg)


def parser_report(line: int, col: int, where: str, msg: str) -> None:
    print(f"{line}:{col} | {msg} {where}")
