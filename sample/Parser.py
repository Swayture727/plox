from typing import Callable, Final, List, Self, Optional

from Expr import Binary, Expr, Grouping, Literal, Unary
from Token import Token, TokenType
from errorReport import parser_error

# Grammar notation	    Code representation
# Terminal	            Code to match and consume a token
# Nonterminal	        Call to that rule’s function
# |	                    if or switch statement
# * or +	            while or for loop
# ? 	                if statement


# TODO: add a ternary operator as a tokentype and add 
# a matching rule with the right precedence

# expression     → equality ;
# equality       → comparison ( ( "!=" | "==" ) comparison )* ;
# comparison     → term ( ( ">" | ">=" | "<" | "<=" ) term )* ;
# term           → factor ( ( "-" | "+" ) factor )* ;
# factor         → unary ( ( "/" | "*" ) unary )* ;
# unary          → ( "!" | "-" ) unary | primary ;
# primary        → NUMBER | STRING | "true" | "false" | "nil" | "(" expression ")" ;
class ParserException(BaseException):
    pass


class Parser:
    _tokens: Final[List[Token]]
    _current: int = 0

    def __init__(self, tokens: List[Token]) -> None:
        self._tokens = tokens

    def parse(self) -> Optional[Expr]:
        try:
            return self._expression()
        except ParserException as _:
            return None

    def _is_at_end(self) -> bool:
        return self._peek_at_current().type == TokenType.EOF

    def _advance(self) -> None:
        if self._is_at_end():
            return
        self._current += 1

    def _look_at_next(self, *types: TokenType) -> bool:
        type = self._peek_at_current().type
        if type in types:
            self._advance()
            return True
        return False

    def _left_asociative_binary_op(
        self, expr_type_method: Callable[[Self], Expr], *operators: TokenType
    ) -> Expr:
        expr: Expr = expr_type_method(self)
        while self._look_at_next(*operators):
            operator: Token = self._previous()
            right: Expr = expr_type_method(self)
            expr = Binary(left=expr, operator=operator, right=right)
        return expr

    def _synchronize(self) -> None:
        self._advance()
        keywords_to_stop_on = [
            TokenType.FOR,
            TokenType.WHILE,
            TokenType.IF,
            TokenType.FUN,
            TokenType.VAR,
            TokenType.CLASS,
            TokenType.PRINT,
            TokenType.RETURN,
        ]

        while not self._is_at_end():
            if self._previous().type == TokenType.SEMICOLON:
                return
            if self._peek_at_current().type in keywords_to_stop_on:
                return
            self._advance()

    def _previous(self) -> Token:
        return self._tokens[self._current - 1]

    def _peek_at_current(self) -> Token:
        return self._tokens[self._current]

    def _expression(self) -> Expr:
        return self._equality()

    # equality       → comparison ( ( "!=" | "==" ) comparison )* ;
    def _equality(self) -> Expr:
        return self._left_asociative_binary_op(
            Parser._comparison, TokenType.BANG_EQUAL, TokenType.EQUAL_EQUAL
        )

    # comparison     → term ( ( ">" | ">=" | "<" | "<=" ) term )* ;
    def _comparison(self) -> Expr:
        return self._left_asociative_binary_op(
            Parser._term,
            TokenType.GREATER,
            TokenType.GREATER_EQUAL,
            TokenType.LESSER,
            TokenType.LESSER_EQUAL,
        )

    # term           → factor ( ( "-" | "+" ) factor )* ;
    def _term(self) -> Expr:
        return self._left_asociative_binary_op(
            Parser._factor, TokenType.PLUS, TokenType.MINUS
        )

    # factor         → unary ( ( "/" | "*" ) unary )* ;
    def _factor(self) -> Expr:
        return self._left_asociative_binary_op(
            Parser._unary, TokenType.SLASH, TokenType.STAR
        )

    # unary          → ( "!" | "-" ) unary | primary ;
    def _unary(self) -> Expr:
        if self._look_at_next(TokenType.BANG, TokenType.MINUS):
            operator: Token = self._previous()
            right: Expr = self._unary()
            return Unary(operator, right)
        return self._primary()

    # primary        → NUMBER | STRING | "true" | "false" | "nil" | "(" expression ")" ;
    def _primary(self) -> Expr:
        if self._look_at_next(TokenType.FALSE):
            return Literal(False)
        if self._look_at_next(TokenType.TRUE):
            return Literal(True)
        if self._look_at_next(TokenType.NIL):
            return Literal(None)
        if self._look_at_next(TokenType.NUMBER, TokenType.STRING):
            return Literal(self._previous().literal)

        if self._look_at_next(TokenType.LEFT_PAREN):
            expr: Expr = self._expression()
            self._consume(TokenType.RIGHT_PAREN, "Expect ')' after expression.")
            return Grouping(expr)

        raise self._error(self._peek_at_current(), "Expect expression.")

    def _check(self, type: TokenType) -> bool:
        if self._is_at_end():
            return False
        return self._peek_at_current().type == type

    def _consume(self, type: TokenType, msg: str) -> Token:
        if self._check(type):
            token = self._peek_at_current()
            self._advance()
            return token
        raise self._error(self._peek_at_current(), msg)

    def _error(self, token: Token, msg: str) -> ParserException:
        parser_error(token, msg)
        return ParserException()
