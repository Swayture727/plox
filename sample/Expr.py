from __future__ import annotations
from abc import ABC, abstractmethod
from typing import Final, LiteralString, override, List
import numbers

from Token import Token, LiteralValue, TokenType


class Expr:
    @abstractmethod
    def accept[R](self, visitor: Visitor[R]) -> R:
        pass


class Binary(Expr):
    left: Final[Expr]
    operator: Final[Token]
    right: Final[Expr]

    def __init__(self, left: Expr, operator: Token, right: Expr) -> None:
        super().__init__()
        self.left = left
        self.operator = operator
        self.right = right

    @override
    def accept[R](self, visitor: Visitor[R]) -> R:
        return visitor._visit_binary_expr(self)


class Grouping(Expr):
    expression: Final[Expr]

    def __init__(self, expr: Expr) -> None:
        super().__init__()
        self.expression = expr

    @override
    def accept[R](self, visitor: Visitor[R]) -> R:
        return visitor._visit_grouping_expr(self)


class Literal(Expr):
    value: Final[LiteralValue]

    def __init__(self, value: LiteralValue) -> None:
        super().__init__()
        self.value = value

    @override
    def accept[R](self, visitor: Visitor[R]) -> R:
        return visitor._visit_literal_expr(self)


class Unary(Expr):
    operator: Final[Token]
    right: Final[Expr]

    def __init__(self, operator: Token, right: Expr) -> None:
        super().__init__()
        self.operator = operator
        self.right = right

    @override
    def accept[R](self, visitor: Visitor[R]) -> R:
        return visitor._visit_unary_expr(self)


class Visitor[R](ABC):
    @abstractmethod
    def _visit_binary_expr(self, expr: Binary) -> R:
        pass

    @abstractmethod
    def _visit_grouping_expr(self, expr: Grouping) -> R:
        pass

    @abstractmethod
    def _visit_literal_expr(self, expr: Literal) -> R:
        pass

    @abstractmethod
    def _visit_unary_expr(self, expr: Unary) -> R:
        pass


class AstPrinter(Visitor[str]):
    def prettyPrint(self, expr: Expr) -> str:
        return expr.accept(self)

    @override
    def _visit_binary_expr(self, expr: Binary) -> str:
        return self.parenthesize(expr.operator.lexeme, expr.left, expr.right)

    @override
    def _visit_grouping_expr(self, expr: Grouping) -> str:
        return self.parenthesize("group", expr.expression)

    @override
    def _visit_literal_expr(self, expr: Literal) -> str:
        if expr.value == None:
            return "nil"
        return str(expr.value)

    @override
    def _visit_unary_expr(self, expr: Unary) -> str:
        return self.parenthesize(expr.operator.lexeme, expr.right)

    def parenthesize(self, name: str, *exprs: Expr) -> str:
        output: List[str] = ["(", name]
        for expr in exprs:
            output.append(" ")
            output.append(expr.accept(self))
        output.append(")")
        return "".join(output)


class LoxRuntimeError(RuntimeError):
    token: Final[Token]
    msg: str

    def __init__(
        self,
        msg: str,
        token: Token,
        *args: object,
    ) -> None:
        super().__init__(*args)
        self.token = token
        self.msg = msg

    def __str__(self) -> str:
        return f"==Lox runtime exception== '{self.msg}' for token {self.token}"


class Interpreter(Visitor[LiteralValue]):
    def interpret(self, expr: Expr):
        try:
            val: LiteralValue = self._evaluate(expr)
            print(Interpreter.stringify(val))
        except LoxRuntimeError as e:
            raise e
        pass

    def _evaluate(self, expr: Expr):
        return expr.accept(self)

    @staticmethod
    def _is_truthy(literal: LiteralValue) -> bool:
        if literal == None:
            return False
        if isinstance(literal, bool):
            return literal
        return False

    @staticmethod
    def stringify(literal: LiteralValue) -> str:
        if literal == None:
            return "nil"
        return str(literal)

    @override
    def _visit_literal_expr(self, expr: Literal) -> LiteralValue:

        return expr.value

    @override
    def _visit_grouping_expr(self, expr: Grouping) -> LiteralValue:
        return self._evaluate(expr.expression)

    @override
    def _visit_unary_expr(self, expr: Unary) -> LiteralValue:
        value: LiteralValue = self._evaluate(expr.right)
        if expr.operator == TokenType.MINUS:
            Interpreter._check_for_number_operands(value, operator=expr.operator)
            return -float(value)  # type: ignore
        if expr.operator == TokenType.BANG:
            return not Interpreter._is_truthy(value)

    @staticmethod
    def _check_for_number_operands(*operands: LiteralValue, operator: Token) -> None:
        msg = (
            "Operand must be a number"
            if len(operands) == 1
            else "Operands must be numbers."
        )
        for operand in operands:
            if not isinstance(operand, numbers.Number):
                raise LoxRuntimeError(msg, token=operator)

    @staticmethod
    def _check_for_number_or_str_operands(
        operator: Token, *operands: LiteralValue
    ) -> None:
        msg = (
            "Operand must be a number or string."
            if len(operands) == 1
            else "Operands must be numbers or strings."
        )
        try:
            Interpreter._check_for_number_operands(
                operator=operator,
                *operands,
            )
            return
        except LoxRuntimeError as _:
            for operand in operands:
                if not isinstance(operand, str):
                    raise LoxRuntimeError(msg, token=operator)

    @override
    def _visit_binary_expr(self, expr: Binary) -> LiteralValue:
        left = self._evaluate(expr.left)
        right = self._evaluate(expr.right)
        match expr.operator.type:
            case TokenType.SLASH:
                Interpreter._check_for_number_operands(
                    left, right, operator=expr.operator
                )
                return left / right  # type: ignore
            case TokenType.MINUS:
                Interpreter._check_for_number_operands(
                    left, right, operator=expr.operator
                )
                return left - right  # type: ignore
            case TokenType.STAR:
                # an explicit conversion since in python you can multiply a str to repeat it
                Interpreter._check_for_number_operands(
                    left, right, operator=expr.operator
                )
                return left * right  # type: ignore
            case TokenType.PLUS:
                Interpreter._check_for_number_or_str_operands(
                    expr.operator, left, right
                )
                return left + right  # type: ignore
            case TokenType.GREATER:
                Interpreter._check_for_number_operands(
                    left, right, operator=expr.operator
                )
                return float(left) > float(right)  # type: ignore
            case TokenType.GREATER_EQUAL:
                Interpreter._check_for_number_operands(
                    left, right, operator=expr.operator
                )
                return float(left) >= float(right)  # type: ignore
            case TokenType.LESSER:
                Interpreter._check_for_number_operands(
                    left, right, operator=expr.operator
                )
                return float(left) < float(right)  # type: ignore
            case TokenType.LESSER_EQUAL:
                Interpreter._check_for_number_operands(
                    left, right, operator=expr.operator
                )
                return float(left) <= float(right)  # type: ignore
            case TokenType.BANG_EQUAL:
                return left == right
            case TokenType.EQUAL_EQUAL:
                return left != right
