from enum import Enum, auto
from typing import Final, Optional


class TokenType(Enum):
    # Single char
    LEFT_PAREN = auto()
    RIGHT_PAREN = auto()

    LEFT_BRACE = auto()
    RIGHT_BRACE = auto()

    COMMA = auto()
    DOT = auto()
    MINUS = auto()
    PLUS = auto()
    SEMICOLON = auto()
    SLASH = auto()
    STAR = auto()
    EQUAL = auto()

    # Single or double char
    BANG = auto()
    BANG_EQUAL = auto()
    EQUAL_EQUAL = auto()
    GREATER = auto()
    GREATER_EQUAL = auto()
    LESSER = auto()
    LESSER_EQUAL = auto()

    # literals
    IDENTIFIER = auto()
    STRING = auto()
    NUMBER = auto()

    # keywords
    AND = auto()
    CLASS = auto()
    ELSE = auto()
    FALSE = auto()
    FUN = auto()
    FOR = auto()
    IF = auto()
    NIL = auto()
    OR = auto()
    PRINT = auto()
    RETURN = auto()
    SUPER = auto()
    THIS = auto()
    TRUE = auto()
    VAR = auto()
    WHILE = auto()

    EOF = auto()


type LiteralValue = float | str | bool | None


class Token:
    type: Final[TokenType]
    lexeme: Final[str]
    literal: Final[LiteralValue]
    line: Final[int]
    column: Final[int]

    def __init__(
        self,
        type: TokenType,
        lexeme: str,
        literal: LiteralValue,
        line: int,
        column: int,
    ) -> None:
        self.type = type
        self.lexeme = lexeme
        self.literal = literal
        self.line = line
        self.column = column

    def __str__(self) -> str:
        return f"{self.type} {self.lexeme} '{self.literal}'"
